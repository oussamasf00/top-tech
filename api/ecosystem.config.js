module.exports = {
  apps: [
    {
      name: "app",
      script: "app.js",
      instances: "max",
      autorestart: true,
      watch_delay: 15000,
      exec_mode: "cluster",
    },
  ],
};
