runner-log: 
	docker logs gitlab-runner

runner:
	docker run --rm -it  \
  		-v /Users/Shared/gitlab-runner/config:/etc/gitlab-runner \
  		-v /var/run/docker.sock:/var/run/docker.sock \
  		gitlab/gitlab-runner:latest register