# Top Tech

## Containerizing Server App with Nginx

This repository demonstrates how to containerize a server application along with Nginx using Docker.

## Overview

The project consists of two main components:

1. **Server App**: Represents a server application.
2. **Nginx**: Serves as a reverse proxy, forwarding requests from the client to the server app.

The server app and Nginx will be containerized separately, and then they will be orchestrated using Docker Compose.
